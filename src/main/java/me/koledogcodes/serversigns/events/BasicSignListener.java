package me.koledogcodes.serversigns.events;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.economy.account.Account;
import org.spongepowered.api.service.economy.transaction.ResultType;
import org.spongepowered.api.service.economy.transaction.TransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import me.koledogcodes.serversigns.ServerSigns;
import me.koledogcodes.serversigns.configs.FileConfiguration;
import me.koledogcodes.serversigns.configs.JSONConfig;
import me.koledogcodes.serversigns.utili.ChatUtili;
import me.koledogcodes.serversigns.utili.ServerSignTimer;
import me.koledogcodes.serversigns.utili.SignAction;
import me.koledogcodes.serversigns.utili.SignUtili;

public class BasicSignListener {

	private HashMap<UUID, String> signLocation = new HashMap<UUID, String>();
	private HashMap<UUID, List<Boolean>> checkItems = new HashMap<UUID, List<Boolean>>();
	
	//TODO Color Sign
	@Listener (order=Order.FIRST)
	public void onGameStart(ChangeSignEvent e){
		if (FileConfiguration.getMainConfig().getBoolean("color-signs")){
			if (e.getCause().first(Player.class) != null){
				Player player = e.getCause().first(Player.class).get();
				
				if (player.hasPermission("svs.colorsign.use")){
					List<Text> lines = e.getText().asList();
					
					if (lines.isEmpty()){ return; }
					
					for (int i = 0; i < lines.size(); i++){
						e.getText().setElement(i, TextSerializers.FORMATTING_CODE.deserialize(lines.get(i).toPlainSingle()));
					}
					
				}
				
				if (!SignUtili.signSpyUsers.isEmpty()){
					for (UUID uuid: SignUtili.signSpyUsers){
						Player spyer = Sponge.getServer().getPlayer(uuid).get();
						
						if (spyer !=  null){
							String lines = "";
							
							for (Text line: e.getText().asList()){
								lines += line.toPlain() + "\n";
							}
							
							ChatUtili.sendHoverMessage(spyer, "&9[&aSign Spy&9] &a" + player.getName() + " &c@ " + new Date(System.currentTimeMillis()).toString(), lines);
						}
					}
				}
				
			}
		}
	}
	
	//TODO Player Join Game
	@Listener
	public void onPlayerJoin(ClientConnectionEvent.Join e){
		if (e.getCause().first(Player.class) != null){
			Player player = e.getCause().first(Player.class).get();
			
			SignUtili.generatePlayerFile(player.getUniqueId());
			
			if (SignUtili.getUserData(player.getUniqueId()).getBoolean("sign-spy") && !SignUtili.signSpyUsers.contains(player.getUniqueId())){
				SignUtili.signSpyUsers.add(player.getUniqueId());
				ChatUtili.sendPrefixedMessage(player, "&aYou are &ecurrently &ain sign spy mode.");
			}
		}
	}
	
	//TODO Player Leave Game
	@Listener
	public void onPlayerLeave(ClientConnectionEvent.Disconnect e){
		if (e.getCause().first(Player.class) != null){
			Player player = e.getCause().first(Player.class).get();

			if (SignUtili.signSpyUsers.contains(player.getUniqueId())){
				SignUtili.signSpyUsers.remove(player.getUniqueId());
			}
		}
	}
	
	//TODO PlayerBreakBlock
	@Listener
	public void playerBreakSign(InteractBlockEvent.Primary e){
		if (e.getCause().first(Player.class) == null){ return; }
		
		Player player = e.getCause().first(Player.class).get();
		
		if (!SignUtili.validateBlock(e.getTargetBlock().getState().getType())){ return; }
		
		if (!SignUtili.signFileExists(player.getWorld(), e.getTargetBlock().getLocation().get())){ return; }
		
		if (!player.hasPermission("svs.admin.*")){
			ChatUtili.sendPrefixedMessage(player, "&cYou do not have permission to delete signs.");
			e.setCancelled(true);
			return;
		}
		
		if (player.get(Keys.IS_SNEAKING).orElse(false)){
			SignUtili.deleteSign(player.getWorld(), e.getTargetBlock().getLocation().get());
			ChatUtili.sendPrefixedMessage(player, "&aServerSign sign has been deleted!");
		}
		else {
			e.setCancelled(true);
			ChatUtili.sendPrefixedMessage(player, "&aYou must be sneaking to delete serversigns!");
		}
	}
	
	//TODO Inventory Close Event
	@Listener
	public void inventoryCloseEvent(InteractInventoryEvent.Close e){
		if (e.getCause().first(Player.class) != null){
			Player player = e.getCause().first(Player.class).get();
			
			if (signLocation.containsKey(player.getUniqueId())){
				String loc = signLocation.get(player.getUniqueId());
				
				if (loc.startsWith("#")){ loc = loc.substring(1); }
				
				JSONConfig config = SignUtili.getSignData(loc);
				
				List<ItemStack> items = new ArrayList<ItemStack>();
				
				for (Inventory inv: e.getTargetInventory().slots()){
					items.add(inv.peek().orElse(null));
				}
				
				if (signLocation.get(player.getUniqueId()).startsWith("#")){
					config.setValue("trade-items", SignUtili.serializeItemStackArray(items));
				}
				else {
					config.setValue("give-items", SignUtili.serializeItemStackArray(items));
				}
				
				signLocation.remove(player.getUniqueId());
				ChatUtili.sendPrefixedMessage(player, "&aItems have been saved to the sign.");
			}
		}
	}
	
	//TODO Player Click Entity Event
	@Listener
	public void playerInteractEntity(InteractEntityEvent.Secondary e){
		if (e.getCause().first(Player.class) == null){ return; }
		
		Player player = e.getCause().first(Player.class).get();
		
		if (SignUtili.checkPlayerAction(player.getUniqueId())){
			if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.ENTITY_CHECK){
				ChatUtili.sendPrefixedMessage(player, "&aEntity type is: &b" + SignUtili.checkEntity(e.getTargetEntity()));
				SignUtili.clearPlayerActionData(player.getUniqueId());
				return;
			}
		}
		
		if (!SignUtili.validateEntity(e.getTargetEntity())){ return; }
					
		//TODO Sign Actions
		if (SignUtili.checkPlayerAction(player.getUniqueId())){
			if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.LIST){
				if (!SignUtili.signFileExists(player.getWorld(), e.getTargetEntity().getLocation())){ return; }

				SignUtili.printSignInformation(player, player.getWorld(), e.getTargetEntity().getLocation());
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.ADD){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				
				List<Object> actions = config.getListArray("commands");
				
				if (actions == null){ actions = new ArrayList<Object>(); }
				
				actions.add(SignUtili.getPlayerData(player.getUniqueId()));
				
				config.setValue("commands", actions);
				ChatUtili.sendPrefixedMessage(player, "&9Action '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' added to sign.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.TRADE_POKEMON){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				
				config.setValue("trade-pokemon", SignUtili.getPartyPokemon(player, SignUtili.getPlayerData(player.getUniqueId())));
				
				ChatUtili.sendPrefixedMessage(player, "&9Action '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' set to sign.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.REMOVE){
				if (!SignUtili.signFileExists(player.getWorld(), e.getTargetEntity().getLocation())){ return; }

				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				
				List<Object> actions = config.getListArray("commands");
				
				if (Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()) >= actions.size() || Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()) < 0){
					ChatUtili.sendPrefixedMessage(player, "&cThat action line does not exist.");
					return;
				}
				
				actions.remove((int) Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()));
				
				config.setValue("commands", actions);
				ChatUtili.sendPrefixedMessage(player, "&9Action number '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' has been removed from the sign.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.PRICE){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("price", Double.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign price has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + " " + SignUtili.getDefaultCurrency().getName() + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.EXP){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("exp-price", Double.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign exp price has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + " Levels" + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.USES){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("uses", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign uses has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9 uses'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.PERMISSION){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("permission", SignUtili.getPlayerData(player.getUniqueId()));
				ChatUtili.sendPrefixedMessage(player, "&9Sign permission has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.COOLDOWN){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("cooldown", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign delay has now been set to '&a" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.GLOBAL_COOLDOWN){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				config.setValue("global-cooldown", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign global delay has now been set to '&a" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.GIVE_ITEM){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				
				if (config.getObject("give-items") == ""){
					signLocation.put(player.getUniqueId(), SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()));
					player.openInventory(SignUtili.createInventory("Sign Items"), Cause.builder().owner(player).build());
				}
				else {
					signLocation.put(player.getUniqueId(), SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()));
					
					Inventory inv = Inventory.builder().build(ServerSigns.class);
					
					try {
						List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("give-items"));
					
						for (ItemStack i: items){
							if (i == null){ continue; }
							
							inv.offer(i);
						}
					}
					catch (Exception exc){
						exc.printStackTrace();
					}
					
					player.openInventory(inv, Cause.builder().owner(player).build());
					
				}
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.TRADE_ITEM){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
				
				if (config.getObject("trade-items") == ""){
					signLocation.put(player.getUniqueId(), "#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()));
					player.openInventory(SignUtili.createInventory("Sign Items"), Cause.builder().owner(player).build());
				}
				else {
					signLocation.put(player.getUniqueId(), "#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()));
					
					Inventory inv = Inventory.builder().build(ServerSigns.class);
					
					try {
						List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("trade-items"));
					
						for (ItemStack i: items){
							if (i == null){ continue; }
							
							inv.offer(i);
						}
					}
					catch (Exception exc){
						exc.printStackTrace();
					}
					
					player.openInventory(inv, Cause.builder().owner(player).build());
					
				}
			}
			else {
				ChatUtili.sendPrefixedMessage(player, "&cInvalid sign action!");
			}
				
			SignUtili.clearPlayerActionData(player.getUniqueId());
		}
		//TODO Sign Interact
		else {
			if (!SignUtili.validateEntity(e.getTargetEntity())){ return; }
			
			if (!SignUtili.signFileExists(player.getWorld(), e.getTargetEntity().getLocation())){ return; }
		
			e.setCancelled(true);
			
			JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetEntity().getLocation());
			
			//Permission
			if (!player.hasPermission("svs.use.*")){
				if (!player.hasPermission(config.getString("permission"))){
					ChatUtili.sendPrefixedMessage(player, "&cYou do not have permission to use this sign.");
					return;
				}
			}
			
			//Uses
			if (config.getInteger("uses") == 0){
				ChatUtili.sendPrefixedMessage(player, "&cThis sign has no more uses.");
				return;
			}
			
			//Cooldown
			if (config.getLong("global-cooldown") > 0){
				if (config.checkKey("global-cooldown-finish")){
					if (System.currentTimeMillis() < config.getLong("global-cooldown-finish")){
						ChatUtili.sendPrefixedMessage(player, "&cPlease wait " + SignUtili.getTime(config.getLong("global-cooldown-finish") - System.currentTimeMillis()) + " until you can use this sign again.");
						return;
					}
					else {
						config.removeKey("global-cooldown-finish");
						ChatUtili.sendPrefixedMessage(player, "&cThe cooldown is over. You may now use the sign.");
						return;
					}
				}
				else {
					if (!price(config, player)) { return; }
					if (!exp_price(config, player)) { return; }
					if (!tradeItems(config, player)) { return; }
					config.setValue("global-cooldown-finish", System.currentTimeMillis() + config.getLong("global-cooldown"));
				}
			}
			else if (config.getLong("cooldown") > 0){
				JSONConfig userConfig = SignUtili.getUserData(player.getUniqueId());
				
				if (userConfig.checkKey("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()))){
					if (System.currentTimeMillis() < userConfig.getLong("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()))){
						ChatUtili.sendPrefixedMessage(player, "&cPlease wait " + SignUtili.getTime(userConfig.getLong("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation())) - System.currentTimeMillis()) + " until you can use this sign again.");
						return;
					}
					else {
						userConfig.removeKey("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()));
						ChatUtili.sendPrefixedMessage(player, "&cThe cooldown is over. You may now use the sign.");
						return;
					}
				}
				else {
					if (!price(config, player)) { return; }
					if (!exp_price(config, player)) { return; }
					if (!tradeItems(config, player)) { return; }
					userConfig.setValue("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetEntity().getLocation()), (System.currentTimeMillis() + config.getLong("cooldown")));
				}
			}
			else {
				if (!price(config, player)) { return; }
				if (!exp_price(config, player)) { return; }
				if (!tradeItems(config, player)) { return; }
			}
			
			executeActions(config, player);
			
			if (config.getInteger("uses") > 0){
				config.setValue("uses", config.getInteger("uses") - 1);
			}
			
		}
	}
	
	//TODO Player Click Sign Event
	@Listener
	public void playerClickSign(InteractBlockEvent.Secondary e){
		if (e.getCause().first(Player.class) == null){ return; }
		
		Player player = e.getCause().first(Player.class).get();
		
		if (!SignUtili.validateBlock(e.getTargetBlock().getState().getType())){ return; }
					
		//TODO Sign Actions
		if (SignUtili.checkPlayerAction(player.getUniqueId())){
			if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.LIST){
				if (!SignUtili.signFileExists(player.getWorld(), e.getTargetBlock().getLocation().get())){ return; }

				SignUtili.printSignInformation(player, player.getWorld(), e.getTargetBlock().getLocation().get());
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.ADD){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				
				List<Object> actions = config.getListArray("commands");
				
				if (actions == null){ actions = new ArrayList<Object>(); }
				
				actions.add(SignUtili.getPlayerData(player.getUniqueId()));
				
				config.setValue("commands", actions);
				ChatUtili.sendPrefixedMessage(player, "&9Action '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' added to sign.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.INSERT){
				try {
					JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
					
					List<Object> actions = config.getListArray("commands");
					
					if (actions == null){ actions = new ArrayList<Object>(); }
					
					actions.add(SignUtili.getPlayerSubData(player.getUniqueId()), SignUtili.getPlayerData(player.getUniqueId()));
					
					config.setValue("commands", actions);
					ChatUtili.sendPrefixedMessage(player, "&9Action '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' added to sign.");
				}
				catch (Exception exc){
					ChatUtili.sendPrefixedMessage(player, "&cFailed to edit sign data.");
				}
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.EDIT){
				try {
					JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
					
					List<Object> actions = config.getListArray("commands");
					
					if (actions == null){ actions = new ArrayList<Object>(); }
					
					actions.set(SignUtili.getPlayerSubData(player.getUniqueId()), SignUtili.getPlayerData(player.getUniqueId()));
					
					config.setValue("commands", actions);
					ChatUtili.sendPrefixedMessage(player, "&9Action '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' has been set to sign.");
				}
				catch (Exception exc){
					ChatUtili.sendPrefixedMessage(player, "&cFailed to edit sign data.");
				}
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.REMOVE){
				if (!SignUtili.signFileExists(player.getWorld(), e.getTargetBlock().getLocation().get())){ return; }

				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				
				List<Object> actions = config.getListArray("commands");
				
				if (Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()) >= actions.size() || Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()) < 0){
					ChatUtili.sendPrefixedMessage(player, "&cThat action line does not exist.");
					return;
				}
				
				actions.remove((int) Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId()).trim()));
				
				config.setValue("commands", actions);
				ChatUtili.sendPrefixedMessage(player, "&9Action number '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' has been removed from the sign.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.PRICE){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("price", Double.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign price has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + " " + SignUtili.getDefaultCurrency().getName() + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.EXP){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("exp-price", Double.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign exp price has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + " Levels" + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.USES){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("uses", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign uses has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9 uses'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.PLAYER_USES){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("player-uses", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign player-uses has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9 uses'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.PERMISSION){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("permission", SignUtili.getPlayerData(player.getUniqueId()));
				ChatUtili.sendPrefixedMessage(player, "&9Sign permission has now been set to '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.COOLDOWN){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("cooldown", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign delay has now been set to '&a" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.GLOBAL_COOLDOWN){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				config.setValue("global-cooldown", Integer.valueOf(SignUtili.getPlayerData(player.getUniqueId())));
				ChatUtili.sendPrefixedMessage(player, "&9Sign global delay has now been set to '&a" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&9'.");
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.GIVE_ITEM){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				
				if (config.getObject("give-items") == ""){
					signLocation.put(player.getUniqueId(), SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()));
					player.openInventory(SignUtili.createInventory("Sign Items"), Cause.builder().owner(player).build());
				}
				else {
					signLocation.put(player.getUniqueId(), SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()));
					
					Inventory inv = Inventory.builder().build(ServerSigns.class);
					
					try {
						List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("give-items"));
					
						for (ItemStack i: items){
							if (i == null){ continue; }
							
							inv.offer(i);
						}
					}
					catch (Exception exc){
						exc.printStackTrace();
					}
					
					player.openInventory(inv, Cause.builder().owner(player).build());
					
				}
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.TRADE_ITEM){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				
				if (config.getObject("trade-items") == ""){
					signLocation.put(player.getUniqueId(), "#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()));
					player.openInventory(SignUtili.createInventory("Sign Items"), Cause.builder().owner(player).build());
				}
				else {
					signLocation.put(player.getUniqueId(), "#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()));
					
					Inventory inv = Inventory.builder().build(ServerSigns.class);
					
					try {
						List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("trade-items"));
					
						for (ItemStack i: items){
							if (i == null){ continue; }
							
							inv.offer(i);
						}
					}
					catch (Exception exc){
						exc.printStackTrace();
					}
					
					player.openInventory(inv, Cause.builder().owner(player).build());
					
				}
			}
			else if (SignUtili.getPlayerAction(player.getUniqueId()) == SignAction.TRADE_POKEMON){
				JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
				
				config.setValue("trade-pokemon", SignUtili.getPartyPokemon(player, SignUtili.getPlayerData(player.getUniqueId())));
				
				ChatUtili.sendPrefixedMessage(player, "&9Action pokemon trading stats '&a" + SignUtili.getPlayerData(player.getUniqueId()) + "&9' for slots 1-6 set to sign.");
			}
			else {
				ChatUtili.sendPrefixedMessage(player, "&cInvalid sign action!");
			}
				
			SignUtili.clearPlayerActionData(player.getUniqueId());
		}
		//TODO Sign Interact
		else {
			if (!SignUtili.validateBlock(e.getTargetBlock().getState().getType())){ return; }
			
			if (!SignUtili.signFileExists(player.getWorld(), e.getTargetBlock().getLocation().get())){ return; }
		
			e.setCancelled(true);
			
			JSONConfig config = SignUtili.getSignData(player.getWorld(), e.getTargetBlock().getLocation().get());
			
			//Permission
			if (!player.hasPermission("svs.admin.*")){
				if (!player.hasPermission(config.getString("permission").trim())){
					ChatUtili.sendPrefixedMessage(player, "&cYou do not have permission to use this sign.");
					return;
				}
			}
			
			//Uses
			if (config.getInteger("uses") == 0){
				ChatUtili.sendPrefixedMessage(player, "&cThis sign has no more uses.");
				return;
			}
			
			//Uses
			if (config.getInteger("player-uses") > 0){
				JSONConfig userConfig = SignUtili.getUserData(player.getUniqueId());
				int uses = userConfig.getInteger(SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()) + ".uses");
			
				if ((uses + 1) > config.getInteger("player-uses")){
					ChatUtili.sendPrefixedMessage(player, "&cThis sign has no more uses.");
					return;
				}
			}
			
			//Cooldown
			if (config.getLong("global-cooldown") > 0){
				if (config.checkKey("global-cooldown-finish")){
					if (System.currentTimeMillis() < config.getLong("global-cooldown-finish")){
						ChatUtili.sendPrefixedMessage(player, "&cPlease wait " + SignUtili.getTime(config.getLong("global-cooldown-finish") - System.currentTimeMillis()) + " until you can use this sign again.");
						return;
					}
					else {
						config.removeKey("global-cooldown-finish");
						ChatUtili.sendPrefixedMessage(player, "&cThe cooldown is over. You may now use the sign.");
						return;
					}
				}
				else {
					if (!price(config, player)) { return; }
					if (!exp_price(config, player)) { return; }
					if (!tradeItems(config, player)) { SignUtili.loadTradeItemsInv(player, config); return; }
					if (!tradePokemon(config, player)) { return; }
					config.setValue("global-cooldown-finish", System.currentTimeMillis() + config.getLong("global-cooldown"));
				}
			}
			else if (config.getLong("cooldown") > 0){
				JSONConfig userConfig = SignUtili.getUserData(player.getUniqueId());
				
				if (userConfig.checkKey("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()))){
					if (System.currentTimeMillis() < userConfig.getLong("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()))){
						ChatUtili.sendPrefixedMessage(player, "&cPlease wait " + SignUtili.getTime(userConfig.getLong("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get())) - System.currentTimeMillis()) + " until you can use this sign again.");
						return;
					}
					else {
						userConfig.removeKey("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()));
						ChatUtili.sendPrefixedMessage(player, "&cThe cooldown is over. You may now use the sign.");
						return;
					}
				}
				else {
					if (!price(config, player)) { return; }
					if (!exp_price(config, player)) { return; }
					if (!tradeItems(config, player)) { SignUtili.loadTradeItemsInv(player, config); return; }
					if (!tradePokemon(config, player)) { return; }
					userConfig.setValue("cooldown#" + SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()), (System.currentTimeMillis() + config.getLong("cooldown")));
				}
			}
			else {
				if (!price(config, player)) { return; }
				if (!exp_price(config, player)) { return; }
				if (!tradeItems(config, player)) { SignUtili.loadTradeItemsInv(player, config); return; }
				if (!tradePokemon(config, player)) { return; }
			}
			
			executeActions(config, player);
			
			if (config.getInteger("uses") > 0){
				config.setValue("uses", config.getInteger("uses") - 1);
			}
			
			if (config.getInteger("player-uses") > 0){
				JSONConfig userConfig = SignUtili.getUserData(player.getUniqueId());
				int uses = userConfig.getInteger(SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()) + ".uses");
				
				userConfig.setValue(SignUtili.parseLocationToString(player.getWorld(), e.getTargetBlock().getLocation().get()) + ".uses", (uses + 1));
			}
			
		}
	}
	
	public void executeActions(JSONConfig config, Player player){
		//Actions
		List<Object> actions = config.getListArray("commands");
		
		if (actions.isEmpty()){ giveItems(config, player); return; }
		
		ServerSignTimer.registerNewNonRepeatingTimer(new TimerTask() {
			
			@Override
			public void run() {
						
						for (Object action: actions){
							
							action = SignUtili.subRndNumbers(action.toString());
							
							if (action.toString().startsWith("<blank>")){
								ChatUtili.sendMessage(player, action.toString().substring("<blank>".length()).replace("<player>", player.getName()));
							}
							else if (action.toString().startsWith("<broadcast>")){
								for (Player p: Sponge.getServer().getOnlinePlayers()){
									ChatUtili.sendMessage(p, action.toString().substring("<broadcast>".length()).replace("<player>", player.getName()));
								}
							}
							else if (action.toString().startsWith("<server>")){
								Sponge.getCommandManager().process(Sponge.getServer().getConsole(), action.toString().substring("<server>".length()).replace("<player>", player.getName()));
							}
							else if (action.toString().startsWith("<command>")){
								Sponge.getCommandManager().process(player, action.toString().substring("<command>".length()).replace("<player>", player.getName()));
							}
							else if (action.toString().startsWith("<delay>")){
								int seconds = Integer.valueOf(action.toString().substring("<delay>".length()));
								try {
									Thread.sleep(seconds * 1000);
								} 
								catch (InterruptedException e) {
									
								}
							}
							else {
								ChatUtili.sendMessage(player, "&cInvalid action '" + action.toString() + "'.");
							}
						}
						
						giveItems(config, player);
						
						cancel();
			}
		}, 10);
	}
	
	public boolean price(JSONConfig config, Player player){
		if (SignUtili.economyExists()){
			if (config.getDouble("price") > 0){
				Account account = SignUtili.getAccount(player.getUniqueId());
				
				TransactionResult result = account.withdraw(SignUtili.getDefaultCurrency(), BigDecimal.valueOf(config.getDouble("price")), Cause.source(this).build());
					if (result.getResult() == ResultType.SUCCESS){
					    ChatUtili.sendPrefixedMessage(player, "&b" + config.getDouble("price") + " " + SignUtili.getDefaultCurrency().getName() + " &ahas been withdrawn from your account.");
					    return true;
					} 
					else if (result.getResult() == ResultType.FAILED || result.getResult() == ResultType.ACCOUNT_NO_FUNDS) {
					    ChatUtili.sendMessage(player, "&cYou need " + (config.getDouble("price") - SignUtili.getBalance(player.getUniqueId())) + SignUtili.getDefaultCurrency().getName() + " " + SignUtili.getDefaultCurrency().getName() + " to use this sign.");
						return false;
					} 
					else {
					    ChatUtili.sendPrefixedMessage(player, "&4&lError: &cCould not withdraw money from account.");
					    return false;
					}
			}
			else {
				return true;
			}
		}
		else {
			ChatUtili.sendMessage(player, "&cNo economy plugin found...");
			return true;
		}
	}
	
	public boolean exp_price(JSONConfig config, Player player){
			if (config.getInteger("exp-price") > 0){
				if (player.get(Keys.EXPERIENCE_LEVEL).get() >= config.getInteger("exp-price")){
					player.offer(Keys.EXPERIENCE_LEVEL, player.get(Keys.EXPERIENCE_LEVEL).get() - config.getInteger("exp-price"));
				    ChatUtili.sendPrefixedMessage(player, "&b" + config.getDouble("exp-price") + " Levels " + " &ahas been taken from your exp bar.");
					return true;
				}
				else {
					ChatUtili.sendPrefixedMessage(player, "&cYou need &4" + (config.getInteger("exp-price") - player.get(Keys.EXPERIENCE_LEVEL).get()) + " Levels &cuntil you can use this sign.");
					return false;
				}
			}
			else {
				return true;
			}
	}
	
	private void giveItems(JSONConfig config, Player player){
		if (config.getObject("give-items") != ""){
			List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("give-items"));
			
			for (ItemStack item: items){
				if (item == null){ continue; }
				
				player.getInventory().offer(item);
			}
		}
	}
	
	private boolean tradeItems(JSONConfig config, Player player){
		if (config.getObject("trade-items") != ""){
			List<ItemStack> items = SignUtili.deserializeItemStackArray(config.getJSONArray("trade-items"));
			
			checkItems.put(player.getUniqueId(), new ArrayList<Boolean>());
			
			//Check Items
			for (ItemStack item: items){
				if (item == null){ continue; }
				
				if (SignUtili.checkItem(player, item)){
					checkItems.get(player.getUniqueId()).add(true);
				}
				else {
					ChatUtili.sendPrefixedItemHoverMessage(player, "&cYou do not have the required amount of items.", item);
					return false;
				}
			}
			
			if (checkItems.get(player.getUniqueId()).contains(false)){
				ChatUtili.sendPrefixedMessage(player, "&cYou do not have the required amount of items.");
				return false;
			}
			
			//Remove Items
			for (ItemStack item: items){
				if (item == null){ continue; }
				
				SignUtili.removeItem(player, item);
			}	
			
			return true;
		}
		else {
			return true;
		}
	}
	
	private boolean tradePokemon(JSONConfig config, Player player){
		if (config.hasKey("trade-pokemon")){
			if (config.getObject("trade-pokemon") == ""){ return true; }
			
			JSONArray array = config.getJSONArray("trade-pokemon");
			
			if (array == null){ return true; }
			
			checkItems.put(player.getUniqueId(), new ArrayList<Boolean>());
			
			//Check Pokemon
			for (int i = 0; i < array.length(); i++){
				JSONObject obj;
				try {
					obj = array.getJSONObject(i);
				} catch (JSONException e) {
					ChatUtili.sendPrefixedMessage(player, "&4&lError: &cSearching for pokemon.");
					return false;
				}
				
				if (obj == null){ continue; }
				
				if (SignUtili.checkPokemon(player, obj)){
					checkItems.get(player.getUniqueId()).add(true);
				}
				else {
					ChatUtili.sendPrefixedPokemonHoverMessage(player, "&cYou do not have the required pokemon.", obj);
					return false;
				}
			}
			
			if (SignUtili.getNumberOfPokemon(player) < 2){
				ChatUtili.sendPrefixedMessage(player, "&cYou must have at least &42 &cpokemon in your party!");
				return false;
			}
			
			if (checkItems.get(player.getUniqueId()).contains(false)){
				ChatUtili.sendPrefixedMessage(player, "&cYou do not have the required pokemon.");
				return false;
			}
			
			//Remove Pokemon
			for (int i = 0; i < array.length(); i++){
				JSONObject obj;
				try {
					obj = array.getJSONObject(i);
				} 
				catch (JSONException e) {
					ChatUtili.sendPrefixedMessage(player, "&4&lError: &cSearching for pokemon.");
					return false;
				}
				
				if (obj == null){ continue; }
				
				SignUtili.removePokemon(player, obj);
			}
			
			return true;
		}
		else {
			return true;
		}
	}
}
