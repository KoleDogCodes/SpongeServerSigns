package me.koledogcodes.serversigns.utili;

import org.json.JSONObject;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.meta.ItemEnchantment;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.Text.Builder;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.serializer.TextSerializers;

import me.koledogcodes.serversigns.configs.FileConfiguration;

public class ChatUtili {

	public ChatUtili() {

	}
	
	final static String prefix = FileConfiguration.getMainConfig().getString("prefix");
	
	public static void sendPrefixedMessage(CommandSource source, String message){
		source.sendMessage( TextSerializers.FORMATTING_CODE.deserialize(prefix + " " + message) );
	}
	
	public static void sendMessage(CommandSource source, String message){
		source.sendMessage( TextSerializers.FORMATTING_CODE.deserialize(message) );
	}
	
	public static void sendPrefixedHoverMessage(CommandSource source, String message, String hoverMessage){
		Builder t = TextSerializers.FORMATTING_CODE.deserialize(prefix + " " + message).toBuilder();
		TextActions.showText(TextSerializers.FORMATTING_CODE.deserialize(hoverMessage)).applyTo(t);
		source.sendMessage(t.toText());
	}
	
	public static void sendHoverMessage(CommandSource source, String message, String hoverMessage){
		Builder t = TextSerializers.FORMATTING_CODE.deserialize(message).toBuilder();
		TextActions.showText(TextSerializers.FORMATTING_CODE.deserialize(hoverMessage)).applyTo(t);
		source.sendMessage(t.toText());
	}
	
	public static void sendPrefixedItemHoverMessage(CommandSource source, String message, ItemStack hoverMessage){
		Builder t = TextSerializers.FORMATTING_CODE.deserialize(prefix + " " + message + " &4(HOVER)").toBuilder();
		TextActions.showText(getItemHoverMessage(hoverMessage)).applyTo(t);
		source.sendMessage(t.toText());
	}
	
	public static void sendPrefixedPokemonHoverMessage(CommandSource source, String message, JSONObject hoverMessage){
		Builder t = TextSerializers.FORMATTING_CODE.deserialize(prefix + " " + message + " &4(HOVER)").toBuilder();
		TextActions.showText(getPokemonHoverMessage(hoverMessage)).applyTo(t);
		source.sendMessage(t.toText());
	}
	
	private static Text getPokemonHoverMessage(JSONObject obj){
		String s = "";
		
		try {
			if (obj.has("name")){
				s += "&7Nickname: &a" + obj.get("name") + "\n" + "\n";
			}
			
			if (obj.has("type")){
				s += "&7Pokemon Type: &a" + obj.get("type") + "\n" + "\n";
			}
			
			if (obj.has("lvl")){
				s += "&7Level: &e" + obj.get("lvl") + "\n";
			}
			if (obj.has("nature")){
				s += "&7Nature: &e" + obj.get("nature") + "\n";
			}
			if (obj.has("ability")){
				s += "&7Ability: &e" + obj.get("ability") + "\n";
			}
			if (obj.has("growth")){
				s += "&7Growth: &e" + obj.get("growth") + "\n";
			}
			if (obj.has("gender")){
				s += "&7Gender: &e" + obj.get("gender") + "\n";
			}
			if (obj.has("shiny")){
				s += "&7Shiny: &e" + obj.get("shiny") + "\n";
			}
			if (obj.has("moveset")){
				JSONObject moves = obj.getJSONObject("moveset");
				
				s += "&7Moveset: &e";
				for (int z = 1; z < 5; z++){
					String move = moves.getString(String.valueOf(z));
					s += move + ", ";
				}
			}
			
			s = s.trim().substring(0, s.length() - 2);
		}
		catch (Exception e){
			
		}
		
		return TextSerializers.FORMATTING_CODE.deserialize(s);
	}
	
	private static Text getItemHoverMessage(ItemStack item){
		String s = "";
		
		if (item.getValue(Keys.DISPLAY_NAME).isPresent()){
			s += "&7Display Name: &e" + TextSerializers.FORMATTING_CODE.serialize(item.getValue(Keys.DISPLAY_NAME).get().get() ) + "\n";
		}
		
		s += "&7Item Type: &e" + item.getItem().getName().replace("minecraft:", "").replace("pixelmon:", "") + "\n";
		s += "&7Amount: &a" + item.getQuantity()+ "\n";
		
		if (item.getValue(Keys.STORED_ENCHANTMENTS).isPresent()){
			s += "\n&7Enchants:" + "\n";
			
			for (ItemEnchantment t: item.getValue(Keys.STORED_ENCHANTMENTS).get().get()){
				s += TextSerializers.FORMATTING_CODE.serialize(Text.of("&e" + t.getEnchantment().getId().split(":")[1] + " &a" + SignUtili.RomanNumerals(t.getLevel()))) + "\n";
			}
		}
		
		if (item.getValue(Keys.ITEM_LORE).isPresent()){
			s += "\n&7Lore:" + "\n";
			
			for (Text t: item.getValue(Keys.ITEM_LORE).get().get()){
				s += TextSerializers.FORMATTING_CODE.serialize(t) + "\n";
			}
		}
		
		return TextSerializers.FORMATTING_CODE.deserialize(s);
	}
}
