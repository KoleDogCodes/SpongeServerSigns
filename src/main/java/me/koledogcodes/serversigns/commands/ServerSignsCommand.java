package me.koledogcodes.serversigns.commands;

import java.util.List;
import java.util.Optional;

import org.spongepowered.api.command.CommandCallable;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import me.koledogcodes.serversigns.configs.JSONConfig;
import me.koledogcodes.serversigns.utili.ChatUtili;
import me.koledogcodes.serversigns.utili.SignAction;
import me.koledogcodes.serversigns.utili.SignUtili;

public class ServerSignsCommand implements CommandCallable {

	@Override
	public Optional<Text> getHelp(CommandSource arg0) {	
		return null;
	}

	@Override
	public Optional<Text> getShortDescription(CommandSource arg0) {
		return null;
	}

	@Override
	public List<String> getSuggestions(CommandSource arg0, String arg1, Location<World> arg2) throws CommandException {
		return null;
	}

	@Override
	public Text getUsage(CommandSource arg0) {
		return null;
	}

	@Override
	public CommandResult process(CommandSource source, String command) throws CommandException {
		if (!(source instanceof Player)){
			ChatUtili.sendMessage(source, "&cCommand can only be used ingame.");
			return CommandResult.success();
		}
		
		if (!source.hasPermission("svs.admin.*")){
			ChatUtili.sendPrefixedMessage(source, "&cYou do not have permission to use this command.");
			return CommandResult.success();
		}
		
		final String[] args = command.trim().split(" ");
		
		Player player = (Player) source;
		
		if (args.length == 1){
			if (args[0].equalsIgnoreCase("list")){
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.LIST);
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a serversign sign.");
			}
			else if (args[0].equalsIgnoreCase("add")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs add <args..>", getAddUsgae());
			}
			else if (args[0].equalsIgnoreCase("insert")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs insert <line> <args..>", "&bInserts a new action to a sign.");
			}
			else if (args[0].equalsIgnoreCase("edit")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs edit <line> <args..>", "&bEdits an action from a sign.");
			}
			else if (args[0].equalsIgnoreCase("remove")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs remove <line>", "&bRemoves a action from a sign.");
			}
			else if (args[0].equalsIgnoreCase("price")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs price <price>", "&bSets a price to the sign.");
			}
			else if (args[0].equalsIgnoreCase("exp")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs exp <price>", "&bSets a exp price to the sign.");
			}
			else if (args[0].equalsIgnoreCase("uses")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs uses <price>", "&bSets a maximum number of uses for the sign.");
			}
			else if (args[0].equalsIgnoreCase("player-uses")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs player-uses <uses>", "&bSets a maximum number of uses per player for the sign.");
			}
			else if (args[0].equalsIgnoreCase("cooldown")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs cooldown <time>", "&bSets a cooldown per player for the sign. \n&6Accepted time formats: &cy,yrs,d,day,hr,hrs,m,mins,s,secs");
			}
			else if (args[0].equalsIgnoreCase("global-cooldown")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs global-cooldown <time>", "&bSets a global cooldown for the sign. \n&6Accepted time formats: &cy,yrs,d,day,hr,hrs,m,mins,s,secs");
			}
			else if (args[0].equalsIgnoreCase("permission")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs permission <permission>", "&bSets a custom permission to the sign.");
			}
			else if (args[0].equalsIgnoreCase("give-items")){
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.GIVE_ITEM);
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to add giveable items to.");
			}
			else if (args[0].equalsIgnoreCase("trade-items")){
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.TRADE_ITEM);
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set tradeable items to.");
			}
			else if (args[0].equalsIgnoreCase("check-entity")){
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.ENTITY_CHECK);
				ChatUtili.sendPrefixedMessage(player, "&aPlease click on a stationary entity.");
			}
			else if (args[0].equalsIgnoreCase("reload")){
				ChatUtili.sendPrefixedMessage(player, "&aCached files are being reloaded...");
				JSONConfig.reloadAllFile();
				ChatUtili.sendPrefixedMessage(player, "&aAll cached files have been reloaded.");
			}
			else if (args[0].equalsIgnoreCase("trade-pokemon")){
				ChatUtili.sendHoverMessage(source, "&c/svs trade-pokemon <args..>", getPokemonTradeUsgae());
			}
			else {
				//Print List Of CMD's
				ChatUtili.sendMessage(source, "&f----------&6 ServerSigns &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs list", "&bPrints out information of sign");
				ChatUtili.sendHoverMessage(source, "&c/svs add <args..>", getAddUsgae());
				ChatUtili.sendHoverMessage(source, "&c/svs trade-pokemon <args..>", getPokemonTradeUsgae());
				ChatUtili.sendHoverMessage(source, "&c/svs insert <line> <args..>", "&bInserts a new action to a sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs edit <line> <args..>", "&bEdits a action from a sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs remove <line>", "&bRemoves a action from a sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs price <price>", "&bSets a price to the sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs exp <price>", "&bSets a exp price to the sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs uses <uses>", "&bSets a maximum number of uses for the sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs player-uses <uses>", "&bSets a maximum number of uses per player for the sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs cooldown <time>", "&bSets a cooldown per player for the sign. \n&6Accepted time formats: &cy,yrs,d,day,hr,hrs,m,mins,s,secs");
				ChatUtili.sendHoverMessage(source, "&c/svs global-cooldown <time>", "&bSets a global cooldown for the sign. \n&6Accepted time formats: &cy,yrs,d,day,hr,hrs,m,mins,s,secs");
				ChatUtili.sendHoverMessage(source, "&c/svs permission <permission>", "&bSets a custom permission to the sign.");
				ChatUtili.sendHoverMessage(source, "&c/svs give-items", "&bGives saved items to a player.");
				ChatUtili.sendHoverMessage(source, "&c/svs trade-items", "&bMakes sign collect items from player.");
				ChatUtili.sendHoverMessage(source, "&c/svs check-entity", "&bChecks an entity's type.");
				ChatUtili.sendHoverMessage(source, "&c/svs reload", "&bReloads all the configuration files.");
			}
		}
		
		if (args.length == 2){
			if (args[0].equalsIgnoreCase("add")){
				
				StringBuilder builder = new StringBuilder();
				builder.append(args[1]);
				
				if (builder.toString().trim().contains("\",\"")){
					ChatUtili.sendPrefixedMessage(source, "&4&lSorry, &cyou cannot have '" + "\",\"" + "' in any action.");
					return CommandResult.success();
				}
				
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.ADD);
				
				SignUtili.setPlayerData(player.getUniqueId(), builder.toString().trim());
				
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to add '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else if (args[0].equalsIgnoreCase("trade-pokemon")){
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.TRADE_POKEMON);
				
				SignUtili.setPlayerData(player.getUniqueId(), args[1].toLowerCase());
				
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set party pokemon trading '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else if (args[0].equalsIgnoreCase("insert")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs insert <line> <args..>", "&bInserts a new action to a sign.");
			}
			else if (args[0].equalsIgnoreCase("edit")){
				ChatUtili.sendMessage(source, "&f----------&6 Command Usage &f----------");
				ChatUtili.sendHoverMessage(source, "&c/svs edit <line> <args..>", "&bEdits an action from a sign.");
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("remove")){
				if (isInt(args[1])){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.REMOVE);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to remove action number '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' from.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is not a whole number.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("price")){
				if (isDouble(args[1])){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.PRICE);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set price '&b" + SignUtili.getPlayerData(player.getUniqueId()) + " " + SignUtili.getDefaultCurrency().getName() + "&a' to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is not a number.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("exp")){
				if (isInt(args[1])){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.EXP);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set '&b" + SignUtili.getPlayerData(player.getUniqueId()) + " Levels &a' to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is not a number.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("cooldown")){
				if (getDelay(args[1]) >= 0){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.COOLDOWN);
					SignUtili.setPlayerData(player.getUniqueId(), String.valueOf(getDelay(args[1])));
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set '&b" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&a' delay to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is an invalid time.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("uses")){
				if (isInt(args[1])){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.USES);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' number of uses to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is not a whole number.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("player-uses")){
				if (isInt(args[1])){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.PLAYER_USES);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' number of uses to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is not a whole number.");
				}
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("permission")){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.PERMISSION);
					SignUtili.setPlayerData(player.getUniqueId(), args[1]);
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set permission '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else if (args.length == 2 && args[0].equalsIgnoreCase("global-cooldown")){
				if (getDelay(args[1]) >= 0){
					SignUtili.setPlayerAction(player.getUniqueId(), SignAction.GLOBAL_COOLDOWN);
					SignUtili.setPlayerData(player.getUniqueId(), String.valueOf(getDelay(args[1])));
					ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to set '&b" + SignUtili.getTime(Long.valueOf(SignUtili.getPlayerData(player.getUniqueId()))) + "&a' global delay to.");
				}
				else {
					ChatUtili.sendPrefixedMessage(source, "&c" + args[1] + " is an invalid time.");
				}
			}
			else {
				ChatUtili.sendPrefixedMessage(source, "&cInvalid arguments.");
			}
			
			return CommandResult.success();
		}
		
		if (args.length >= 2){
			if (args[0].equalsIgnoreCase("add")){
				
				StringBuilder builder = new StringBuilder();
				for (int i = 1; i < args.length; i++){
					builder.append(args[i] + " ");
				}
				
				if (builder.toString().trim().contains("\",\"")){
					ChatUtili.sendPrefixedMessage(source, "&4&lSorry, &cyou cannot have '" + "\",\"" + "' in any action.");
					return CommandResult.success();
				}
				
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.ADD);
				
				SignUtili.setPlayerData(player.getUniqueId(), builder.toString().trim());
				
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to add '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else if (args[0].equalsIgnoreCase("insert") && args.length >= 3){
				int line = -1;
				
				try {
					line = Integer.parseInt(args[1]);
					
					if (line < 0){
						ChatUtili.sendPrefixedMessage(source, "&cInvalid integer!");
						return CommandResult.success();
					}
				}
				catch (Exception e){
					line = -1;
					ChatUtili.sendPrefixedMessage(source, "&cInvalid integer!");
					return CommandResult.success();
				}
				
				
				StringBuilder builder = new StringBuilder();
				for (int i = 2; i < args.length; i++){
					builder.append(args[i] + " ");
				}
				
				if (builder.toString().trim().contains("\",\"")){
					ChatUtili.sendPrefixedMessage(source, "&4&lSorry, &cyou cannot have '" + "\",\"" + "' in any action.");
					return CommandResult.success();
				}
				
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.INSERT);
				
				SignUtili.setPlayerData(player.getUniqueId(), builder.toString().trim());
				SignUtili.setPlayerSubData(player.getUniqueId(), line);
				
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to insert '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else if (args[0].equalsIgnoreCase("edit") && args.length >= 3){
				int line = -1;
				
				try {
					line = Integer.parseInt(args[1]);
					
					if (line < 0){
						ChatUtili.sendPrefixedMessage(source, "&cInvalid integer!");
						return CommandResult.success();
					}
				}
				catch (Exception e){
					line = -1;
					ChatUtili.sendPrefixedMessage(source, "&cInvalid integer!");
					return CommandResult.success();
				}
				
				
				StringBuilder builder = new StringBuilder();
				for (int i = 2; i < args.length; i++){
					builder.append(args[i] + " ");
				}
				
				if (builder.toString().trim().contains("\",\"")){
					ChatUtili.sendPrefixedMessage(source, "&4&lSorry, &cyou cannot have '" + "\",\"" + "' in any action.");
					return CommandResult.success();
				}
				
				SignUtili.setPlayerAction(player.getUniqueId(), SignAction.EDIT);
				
				SignUtili.setPlayerData(player.getUniqueId(), builder.toString().trim());
				SignUtili.setPlayerSubData(player.getUniqueId(), line);
				
				ChatUtili.sendPrefixedMessage(player, "&aPlease click a sign to edit '&b" + SignUtili.getPlayerData(player.getUniqueId()) + "&a' to.");
			}
			else {
				ChatUtili.sendPrefixedMessage(source, "&cInvalid argument '" + args[0] + "'.");
			}
			
		}
		
		
		return CommandResult.success();
	}

	@Override
	public boolean testPermission(CommandSource arg0) {
		return arg0.hasPermission("serversigns.admin.*");
	}
	
	public String getAddUsgae(){
		return "" +
			"/svs add <server>(command) &f- &6Add cmd to sign (Executed by console)\n" + 
			"/svs add <command>(command) &f- &6Add cmd to sign (Executed by player)\n" + 
			"/svs add <blank>(message) &f- &6Sends message to player\n" + 
			"/svs add <broadcast>(message) &f- &6Sends message to all players\n" + 
			"/svs add <delay>(seconds) &f- &6Delays messages and commands\n\n" + 
			"&f&lPlaceholders\n" +
			"&6<player> &a- Gets the player's username"
		;
	}
	
	public String getPokemonTradeUsgae(){
		return "" +
			"/svs trade-pokemon <args,args1...> &f- &6Allows pokemon trading\n" + 
			"&c&lNote: &7This uses all the pokemon in your party.\n" +
			"&c&lNote: &7The values will automatically be obtained do not input the pokemon's actual lvl, nature etc...\n" +
			"\n&f&lCriteria\n" +
			"&6lvl &a- Saves the pokemon's level\n" + 
			"&6name &a- Saves the pokemon's nickname\n" + 
			"&6type &a- Saves the pokemon's type\n" + 
			"&6nature &a- Saves the pokemon's nature\n" +
			"&6ability &a- Saves the pokemon's ability\n" + 
			"&6growth &a- Saves the pokemon's growth\n" + 
			"&6gender &a- Saves the pokemon's gender\n" + 
			"&6moveset &a- Saves the pokemon's moveset\n" + 
			"&6shiny &a- Saves whether or not pokemon is shiny\n"
		;
	}
	
	public boolean isInt(String s){
		try {
			Integer.parseInt(s);
			return true;
		}
		catch (Exception e){
			return false;
		}
	}
	
	public boolean isDouble(String s){
		try {
			Double.parseDouble(s);
			return true;
		}
		catch (Exception e){
			return false;
		}
	}
	
	public long getDelay(String s){
		s = s.toLowerCase();
		
		if (!isDouble(removeTimePrefix(s))){ return -1; }
		
		try {
			if (s.endsWith("y") || s.endsWith("yr") || s.endsWith("year") || s.endsWith("yrs") || s.endsWith("years")){
				return Long.parseLong(removeTimePrefix(s)) * (1000 * 60 * 60 * 24 * 365);
			}
			else if (s.endsWith("d") || s.endsWith("day")){
				return Long.parseLong(removeTimePrefix(s)) * (1000 * 60 * 60 * 24);
			}
			else if (s.endsWith("h") || s.endsWith("hr") || s.endsWith("hour") || s.endsWith("hours") || s.endsWith("hrs")){
				return Long.parseLong(removeTimePrefix(s)) * (1000 * 60 * 60);
			}
			else if (s.endsWith("min") || s.endsWith("minutes") || s.endsWith("m")){
				return Long.parseLong(removeTimePrefix(s)) * (1000 * 60);
			}
			else if (s.endsWith("s") || s.endsWith("sec") || s.endsWith("secs") || s.endsWith("second") || s.endsWith("seconds")){
				return Long.parseLong(removeTimePrefix(s)) * (1000);
			}
			else {
				return -1;
			}
		}
		catch (Exception e){
			return -1;
		}
	}
	
	private String removeTimePrefix(String s){
		return s.
			replace("y", "").
			replace("yr", "").
			replace("yrs", "").
			replace("year", "").
			replace("years", "").
			replace("d", "").
			replace("day", "").
			replace("h", "").
			replace("hr", "").
			replace("hrs", "").
			replace("hour", "").
			replace("hours", "").
			replace("min", "").
			replace("minutes", "").
			replace("m", "").
			replace("s", "").
			replace("sec", "").
			replace("secs", "").
			replace("second", "").
			replace("seconds", "")
			;
	}

}
